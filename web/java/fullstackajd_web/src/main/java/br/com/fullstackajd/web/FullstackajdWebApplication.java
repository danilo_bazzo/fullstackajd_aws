package br.com.fullstackajd.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FullstackajdWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(FullstackajdWebApplication.class, args);
	}

}
