import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ProdutoModel } from "../model/produto.model";

@Injectable({providedIn:'root'})
export class ProdutoService{
    baseUrl = 'http://fullstackajd-aws-produto.sa-east-1.elasticbeanstalk.com/produto';

    constructor(private http: HttpClient){ }

    public find(): Observable<ProdutoModel[]>
    {
        return this.http.get<ProdutoModel[]>(this.baseUrl);
    }
}