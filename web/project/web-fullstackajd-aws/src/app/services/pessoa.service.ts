import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { PessoaModel } from "../model/pessoa.model";

@Injectable({providedIn:'root'})
export class PessoaService{
    baseUrl = 'http://fullstackajdawspessoa-env.eba-n3pymmhu.sa-east-1.elasticbeanstalk.com/pessoa';

    constructor(private http: HttpClient){ }

    public find(): Observable<PessoaModel[]>
    {
        return this.http.get<PessoaModel[]>(this.baseUrl);
    }
}