import { Component, OnInit } from '@angular/core';
import { ProdutoModel } from 'src/app/model/produto.model';
import { ProdutoService } from 'src/app/services/produto.service';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {

  constructor(private service : ProdutoService) { }

  dataSource: ProdutoModel[]=[];

  ngOnInit(): void {
    this.loadComponent();
  }

  public loadComponent() : void{
    this.dataSource = [];

    this.service.find().subscribe((result: ProdutoModel[]) => {
      this.dataSource = result;
    }, err =>{
      this.dataSource = [];
    });

  }

}
