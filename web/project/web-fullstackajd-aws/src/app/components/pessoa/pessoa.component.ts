import { Component, OnInit } from '@angular/core';
import { PessoaModel } from 'src/app/model/pessoa.model';
import { PessoaService } from 'src/app/services/pessoa.service';

@Component({
  selector: 'app-pessoa',
  templateUrl: './pessoa.component.html',
  styleUrls: ['./pessoa.component.css']
})
export class PessoaComponent implements OnInit {

  constructor(private service : PessoaService) { }

  dataSource: PessoaModel[]=[];

  ngOnInit(): void {
    this.loadComponent();
  }

  public loadComponent() : void{
    this.dataSource = [];

    this.service.find().subscribe((result: PessoaModel[]) => {
      this.dataSource = result;
    }, err =>{
      this.dataSource = [];
    });

  }

}
