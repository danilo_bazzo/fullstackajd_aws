﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DmMSProduto.Domain.Entity
{
    public interface IBaseService<T> where T : Base
    {
        IEnumerable<T> Get();

        //void Insert(T obj);
    }
}
