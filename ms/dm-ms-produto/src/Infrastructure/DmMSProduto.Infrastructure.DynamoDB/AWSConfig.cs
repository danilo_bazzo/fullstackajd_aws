﻿using System;

namespace DmMSProduto.Infrastructure.DynamoDB
{
    public class AWSConfig
    {
        public string region { get; set; }
        public string accessKey { get; set; }
        public string secretKey { get; set; }
    }
}
