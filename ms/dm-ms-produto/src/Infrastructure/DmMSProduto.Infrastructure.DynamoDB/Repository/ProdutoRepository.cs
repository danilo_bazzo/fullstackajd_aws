﻿using Amazon.DynamoDBv2;
using Amazon.Runtime;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using DmMSProduto.Domain.Entity;
using DmMSProduto.Infrastructure.DynamoDB.Mapper;
using DmMSProduto.Infrastructure.DynamoDB.Interface;

namespace DmMSProduto.Infrastructure.DynamoDB.Repository
{
    public class ProdutoRepository : IProdutoRepository
    {
        protected readonly AmazonDynamoDBClient _dynamoDBClient;

        public ProdutoRepository(IOptions<AWSConfig> config)
        {
            _dynamoDBClient = new AmazonDynamoDBClient(
                new BasicAWSCredentials(config.Value.accessKey, config.Value.secretKey),
                Amazon.RegionEndpoint.GetBySystemName(config.Value.region)
            );
        }

        public IEnumerable<Produto> Get()
        {
            var task = _dynamoDBClient.ScanAsync("TB_ET_PRODUTO", new List<string>());

            task.Wait();

            if (task.IsCompleted)
                return (IEnumerable<Produto>)ProdutoMapper.MapResultToDomain(task.Result.Items);
            else
                return null;

        }

    }
}
