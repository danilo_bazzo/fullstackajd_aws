﻿using Amazon.DynamoDBv2.Model;
using DmMSProduto.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DmMSProduto.Infrastructure.DynamoDB.Mapper
{
    public class ProdutoMapper
    {
        public static IEnumerable<Produto> MapResultToDomain(List<Dictionary<string, AttributeValue>> result)
        {
            var list = new List<Produto>();

            foreach (var item in result)
                list.Add(new Produto() {
                    Id = item["id"].S,
                    Name = item["name"].S
                });

            return list;
        }
    }
}
