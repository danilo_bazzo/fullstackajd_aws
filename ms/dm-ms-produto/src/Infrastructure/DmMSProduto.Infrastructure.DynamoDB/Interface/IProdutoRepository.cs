﻿using DmMSProduto.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DmMSProduto.Infrastructure.DynamoDB.Interface
{
    public interface IProdutoRepository: IBaseRepository<Produto>
    {

    }
}
