﻿using DmMSProduto.Application.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dm_ms_produto.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly ILogger<ProdutoController> _logger;

        protected IProdutoService _service;
        public ProdutoController(IProdutoService service, ILogger<ProdutoController> logger)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Execute(() => _service.Get());
        }

        private IActionResult Execute(Func<object> func)
        {
            try
            {
                return Ok(func());
            }
            catch(Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
