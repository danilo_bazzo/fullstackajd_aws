﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DmMSProduto.Application.DTO
{
    public class ProdutoDTO
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
