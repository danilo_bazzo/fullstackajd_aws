﻿using DmMSProduto.Application.Interface;
using DmMSProduto.Domain.Entity;
using DmMSProduto.Infrastructure.DynamoDB.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace DmMSProduto.Application.Implementation
{
    public class ProdutoService : IProdutoService
    {
        protected readonly IProdutoRepository _repository;

        public ProdutoService(IProdutoRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Produto> Get() => _repository.Get();
    }
}
