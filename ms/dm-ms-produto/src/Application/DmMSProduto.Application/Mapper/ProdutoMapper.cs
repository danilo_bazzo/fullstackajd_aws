﻿using DmMSProduto.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using DmMSProduto.Application.DTO;

namespace DmMSProduto.Application.Mapper
{
    public class ProdutoMapper
    {
        public static IEnumerable<Produto> MapDTOToDomainList(IEnumerable<ProdutoDTO> list)
        {
            var result = new List<Produto>();

            foreach (var item in list)
                result.Add(MapDTOToDomain(item));
            
            return result;
        }

        public static Produto MapDTOToDomain(ProdutoDTO dto)
        {
            return new Produto()
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }

        public static IEnumerable<ProdutoDTO> MapDomainToDTOList(IEnumerable<Produto> list)
        {
            var result = new List<ProdutoDTO>();

            foreach (var item in list)
                result.Add(MapDomainToDTO(item));

            return result;
        }

        public static ProdutoDTO MapDomainToDTO(Produto domain)
        {
            return new ProdutoDTO()
            {
                Id = domain.Id,
                Name = domain.Name
            };
        }
    }
}
