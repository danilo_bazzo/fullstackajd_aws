package br.com.dm.ms.pessoa.adapter.api.in;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.dm.ms.pessoa.core.PessoaService;
import br.com.dm.ms.pessoa.dto.PessoaDTO;

@Controller
@RequestMapping("pessoa")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PessoaController {

	@Autowired
	private PessoaService service;
	
	@GetMapping
	(
		produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	public ResponseEntity<Object> get()
	{
		try
		{
			List<PessoaDTO> list = service.findAll();
			
			if(!list.isEmpty())
				return new ResponseEntity<Object>(list, HttpStatus.OK);
	
			return new ResponseEntity<Object>("Não há registros!", HttpStatus.NO_CONTENT);
		}
		catch(Exception e)
		{
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
