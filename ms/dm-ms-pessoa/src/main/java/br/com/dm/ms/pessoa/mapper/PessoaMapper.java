package br.com.dm.ms.pessoa.mapper;

import java.util.ArrayList;
import java.util.List;

import br.com.dm.ms.pessoa.dto.PessoaDTO;
import br.com.dm.ms.pessoa.model.PessoaDO;

public class PessoaMapper
{
	
	public static List<PessoaDTO> toDTO(List<PessoaDO> list){
		
		List<PessoaDTO> result = new ArrayList<PessoaDTO>();
		
		for(PessoaDO modal : list)
			result.add(toDTO(modal));
		
		return result;
	}
	
	public static PessoaDTO toDTO(PessoaDO modal){
		PessoaDTO dto = new PessoaDTO();
		dto.id = modal.getId();
		dto.name = modal.getName();
		return dto;
	}

	public static List<PessoaDO> toDO(List<PessoaDTO> list){
		
		List<PessoaDO> result = new ArrayList<PessoaDO>();
		
		for(PessoaDTO dto : list)
			result.add(toDO(dto));
		
		return result;
	}
	
	public static PessoaDO toDO(PessoaDTO dto){
		PessoaDO modal = new PessoaDO();
		modal.setId(dto.id);
		modal.setName(dto.name);
		return modal;
	}
}
