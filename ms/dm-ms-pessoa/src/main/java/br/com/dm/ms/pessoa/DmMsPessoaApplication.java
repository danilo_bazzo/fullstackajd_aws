package br.com.dm.ms.pessoa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DmMsPessoaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DmMsPessoaApplication.class, args);
	}

}
