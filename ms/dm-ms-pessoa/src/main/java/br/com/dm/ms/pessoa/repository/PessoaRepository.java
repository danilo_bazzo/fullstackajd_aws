package br.com.dm.ms.pessoa.repository;

import java.util.Optional;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import br.com.dm.ms.pessoa.model.PessoaDO;

@EnableScan
public interface PessoaRepository extends 
  CrudRepository<PessoaDO, String> {
    
    Optional<PessoaDO> findById(Integer id);
}