package br.com.dm.ms.pessoa.core.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dm.ms.pessoa.core.PessoaService;
import br.com.dm.ms.pessoa.dto.PessoaDTO;
import br.com.dm.ms.pessoa.mapper.PessoaMapper;
import br.com.dm.ms.pessoa.model.PessoaDO;
import br.com.dm.ms.pessoa.repository.PessoaRepository;

@Service
public class PessoaServiceImpl implements PessoaService
{	
	@Autowired
	private PessoaRepository repository;

	public List<PessoaDTO> findAll(){
		
		List<PessoaDO> result = new ArrayList<PessoaDO>();
		Iterable<PessoaDO> iterator = repository.findAll();
		
		iterator.forEach(result::add);
        
		return PessoaMapper.toDTO(result);
	}
}
