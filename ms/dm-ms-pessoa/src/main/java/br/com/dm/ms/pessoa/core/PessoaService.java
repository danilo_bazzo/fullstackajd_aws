package br.com.dm.ms.pessoa.core;

import java.util.List;

import br.com.dm.ms.pessoa.dto.PessoaDTO;

public interface PessoaService
{
	public List<PessoaDTO> findAll();

}
