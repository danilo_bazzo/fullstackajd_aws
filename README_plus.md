* Instalar Java 1.8
* Instalar Maven 
* Instalar STS
* Instalar Node.js
* Instalar VSCode


# Angular:
## Abrir terminal VSCode ou cmd 
* npm install -g @angular/cli
* npm install --save-dev @angular-devkit/build-angular (Caso erro)
* npm cache clean --force
----
caso Restrict ou AllSign (Abrir o PowerShell como ADM
- Get-ExecutionPolicy (para verificar)
Set-ExecutionPolicy RemoteSigned OU Set-ExecutionPolicy ByPass (utilizei o ByPass)
----

## Criar projeto e componentes
* ng new [nome do projeto]
* ng generate component [nome]

* ng s
## Acessar o endereço padrão: http://localhost:4200/

* ng build
