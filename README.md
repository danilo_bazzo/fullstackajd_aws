# Desenvolvendo um "Hello World" Fullstack com Angular, Microservice Java STS (Video 1) e Asp.Net Core (Video 2), AWS Elastic BeanStalk e AWS DynamoDB
----

Minha intenção com esse vídeo, é ajudar aos desenvolvedores a entender que.. uma aplicação em cloud AWS não é nenhum bicho de 7 cabeças, ou seja, só muda a cor da grama mas a complexidade continua a mesma! 
É claro que foi bem simples, bem CRUD ... ops apenas o C do CRUD, rs. Mas a ideia foi ser simples e objetivo mesmo! Esse vídeo tem quase 3h que demonstro todo passo a passo e direto, é por isso que é longo. 
A AWS é uma imensa plataforma que permite distribuir sua aplicação em nano serviços em nuvem. Reforço que o vídeo foi especifico sem expandir para outros assunto, arquiteturas, dockers, filas e afins... 
Espero que com esse vídeo eu tenha ajudado as pessoas, pois eu mesmo tive dificuldades de encontrar um material tão direto assim.  


Youtube: 
Video 1 (Java STS): https://youtu.be/AFSzuPJFaFI
Video 2 (Asp.Net Core): https://youtu.be/wbu2fIgLDpA


Abraço a todos,
Danilo Bazzo

----
* AppWeb: http://fullstackajd-aws-web.sa-east-1.elasticbeanstalk.com/index.html
* API Java: http://fullstackajdawspessoa-env.eba-n3pymmhu.sa-east-1.elasticbeanstalk.com/pessoa
* API .Net: http://fullstackajd-aws-produto.sa-east-1.elasticbeanstalk.com/produto
* Bitbucket: https://bitbucket.org/danilo_bazzo/fullstackajd_aws

#helloworld #dev #desenvolvimento #sistemas #api #rest #restfull #microservice #cloud #cloudcomputer #angular #typescript #aspnet #aspcore #aspnetcore #visualstudio #visualstudiocommunity #microsoft #ddd #java #spring #springboot #sts #elastic #elasticbenstalk #dynamo #dynamodb #aws #amazon